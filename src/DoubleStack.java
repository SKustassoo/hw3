import java.util.StringTokenizer;
import java.util.LinkedList;
import java.util.List;

//http://www.javatpoint.com/string-tokenizer-in-java - tokenite info
//Valdav enamus koodi leongust ja �ppej�u materjalidest

//https://github.com/margusja/DoubleStack/blob/master/src/DoubleStack.java   - viimase commit paranduse info
	public class DoubleStack {

	private LinkedList<Double> theMainList = new LinkedList<Double>();
	
	
   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }
   
   DoubleStack(){
	   
   }

   /*DoubleStack(int stackSize) { magasiiniga oli tarvis seda varianti
	   stack = new double[stackSize];
	   sp = -1;
	   
      // TODO!!! Your constructor here!
   }*/

   @Override
   public Object clone() throws CloneNotSupportedException {
		DoubleStack tmp = new DoubleStack();
        for (int i=theMainList.size() - 1 ; i >= 0 ; i--) {
            tmp.push(theMainList.get(i));
        }
        return tmp;
    //  return this; // TODO!!! Your code here!
   }

   public boolean stEmpty() {
	   
	   return theMainList.isEmpty();
	   /*boolean isEmpty = false;
       // TODO!!! Your code here!
      if(sp == -1){
    	isEmpty = true;}
      return isEmpty;*/
      
   }

   public void push (double a) {
	   theMainList.addFirst(a);
      /* TODO!!! Your code here!
		sp += 1; // increment
		stack[sp] = a;*/
   }

   public double pop() {
		if (stEmpty())
		throw new IndexOutOfBoundsException("Stack Underflow");
		
		
		double tmp = theMainList.getFirst();
		theMainList.removeFirst();
		//sp -= 1;  decrement
		return tmp;
		
       // TODO!!! Your code here!
   } // pop

   public void op (String s) {
	   if (stEmpty())
	         throw new RuntimeException ("Nimekiri on t�hi");
	   
	   double op1 = theMainList.pop();
	   
	   if (stEmpty())
	         throw new RuntimeException ("Nimekiri on t�hi");
	   
	   double op2 = theMainList.pop();
	   
	   if(s.equals("+")){
		   theMainList.push(op2 + op1);
	   }
	   else if(s.equals("-")){
		   theMainList.push(op2 - op1);
	   }
	   else if(s.equals("*")){
		   theMainList.push(op2*op1);
	   }
	   else if(s.equals("/")){
		   if(op1 != 0){
			   theMainList.push(op2/op1);
		   }else{
			   throw new ArithmeticException ("Sisestati jada kus ei saa jagatist teha.");
		   }
			   
	   }else
			throw new RuntimeException("Sisestati m�rk, mida ei tunta");
      /* TODO!!!
		double op2 = pop();
		double op1 = pop();
		
		if (s.equals("+"))
			push(op1 + op2);
		if (s.equals("-"))
			push(op1 - op2);
		if (s.equals("*"))
			push(op1 * op2);
		if (s.equals("/"))
			push(op1 / op2); */
   }  
  
   public double tos() {
		if (stEmpty())
			throw new IndexOutOfBoundsException("Stack Underflow");
		return theMainList.getFirst();
      //return 0.; // TODO!!! Your code here!
   }

   @Override
   public boolean equals (Object o) {
	   if(theMainList.equals(((DoubleStack) o).theMainList)){
		   return true;
	   }else{
		   return false;
	   }
		/*if (((DoubleStack) o).sp != sp)
			return false;
		for (int i = 0; i <= sp; i++)
			if (((DoubleStack) o).stack[i] != stack[i])
				return false;
		return true;
     // return true; // TODO!!! Your code here!*/
   }

   @Override
   public String toString() {
	   if(stEmpty())
		   System.out.println("Nimekiri on t�hi");
	   StringBuffer asText = new StringBuffer();
	   
	   for (int i = theMainList.size()-1;i >= 0; i--)
			asText.append(theMainList.get(i));
	   return asText.toString();

   }

   public static double interpret (String pol) {
	   if (pol == null){
		   throw new RuntimeException("Sisestasid t�hja nimekirja");
	   }
	   if (pol.trim().length() < 1){
		   throw new RuntimeException("Sisestasid t�hja nimekirja");
	   }
	   
	   DoubleStack dbstack = new DoubleStack();
	   StringTokenizer st = new StringTokenizer(pol, " \t");
	   int i = 1;
	   int elemente = st.countTokens();
	
	   while(st.hasMoreTokens()){
		   String token = (String) st.nextElement();

		   
		   if(token.equals("+") || token.equals("-") || token.equals("/")|| token.equals("*")){
			   if(dbstack.theMainList.size() < 2)
				   throw new RuntimeException("Nimekirjas ("+pol+") ei ole piisavalt elemente, et teostada tehet." );
			   
			   if(dbstack.stEmpty())
				   throw new RuntimeException("Nimekirjas: ("+pol+") Element: "+token+" ei saa olla esimene element.");
			   
			   
			   dbstack.op(token);
		   }else{
			   if(elemente == i && i>2) {
				   throw new RuntimeException("Nimekirjas: ("+pol+"). Tehete arv ei vasta elementide arvule.");
			   }
			   try {
				   dbstack.push(Double.parseDouble(token));
			} catch (NumberFormatException e) {
				System.out.println("Sisestatud nimekirjas ("+pol+") olev element "+token+" ei ole avalidses tuvastatav m�rk.");
			}
			  
		   }
		   i++;
	   }
	   return dbstack.tos();
   }

}

